import unittest
import sbh

class Sbh(unittest.TestCase):

    def test_standardize_author(self):
        a1 = "Torra, Vicenç and Navarro-Arribas, Guillermo"
        r1 = "Vicenç Torra, Guillermo Navarro-Arribas"
        a2 = "S. Delgado-Segura and C. Perez-Sola and J. Herrera-Joancomarti and G. Navarro-Arribas"
        r2 = "S. Delgado-Segura, C. Perez-Sola, J. Herrera-Joancomarti, G. Navarro-Arribas"
        a3 = "Navarro-Arribas, G. and Torra, V."
        r3 = "G. Navarro-Arribas, V. Torra"
        a4 = "Guillermo Navarro-Arribas and Daniel Abril and Vicenç Torra"
        r4 = "Guillermo Navarro-Arribas, Daniel Abril, Vicenç Torra"
        a5 = "Alan Turing"
        r5 = "Alan Turing"
        a6 = "Turing, A."
        r6 = "A. Turing"
        b1 = bibhtml.standardize_author(a1)
        print(b1)
        b2 = bibhtml.standardize_author(a2)
        print(b2)
        b3 = bibhtml.standardize_author(a3)
        b4 = bibhtml.standardize_author(a4)
        b5 = bibhtml.standardize_author(a5)
        b6 = bibhtml.standardize_author(a6)
        self.assertEqual(b1, r1)
        self.assertEqual(b2, r2)
        self.assertEqual(b3, r3)
        self.assertEqual(b4, r4)
        self.assertEqual(b5, r5)
        self.assertEqual(b6, r6)

