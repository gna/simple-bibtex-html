#!/usr/bin/env python3
"""
sbh.py converts a set of BibTeX files to a single HTML file using Jinja2
templates to format the bib items.
"""

import argparse
import pathlib
import re
import shutil
import sys

import bibtexparser
import jinja2

__author__ = "Guillermo Navarro-Arribas"


BASE_DIR = pathlib.Path(__file__).absolute().parent
TEMPLATE_DIR = BASE_DIR / "templates"
MACROS_FILE = "macros.jinja2"
FULL_BIB_FILE = BASE_DIR / "files" / "fullbib.bib"
FULL_HTML_FILE = BASE_DIR / "files" / "fullbib.html"


def strip_suffix(name: str, suffix: str) -> str:
    """
    Strips the given suffix from the end of a string.

    :param name: string (e.g. name of a file)
    :param suffix: suffix to be removed
    :return: the string without the suffix
    """
    if name.endswith(suffix):
        return name[: len(name) - len(suffix)]
    return name


def load_templates(
        template_dir: str | pathlib.Path,
        macros_file: str | pathlib.Path
) -> dict[str, jinja2.Template]:
    """
    Loads the templates from the directory templates into a dictionary, where
    the key is the name of the BibTeX entry and the value the corrsponding
    Jinja2 template.

    :return: the dictionary with the templates
    """
    loader = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
    template_files = loader.list_templates()
    template_files.remove(macros_file)
    templates = {}
    for f in template_files:
        templates[strip_suffix(f, ".jinja2")] = loader.get_template(f)
    return templates


def read_bibtex_file(
    filename: str | pathlib.Path
) -> bibtexparser.bibdatabase.BibDatabase:
    """
    Reads a BibTeX file into a bibtexparer db.

    :param filename: the BibTeX file
    :return: bibtexparser database
    """
    parser = bibtexparser.bparser.BibTexParser(common_strings=True)
    parser.customization = bibtexparser.customization.convert_to_unicode
    with open(filename) as f:
        bib_db = parser.parse_file(f)
    return bib_db


def get_all_years(db: bibtexparser.bibdatabase.BibDatabase) -> list[str]:
    """
    Given a bibtexparser DB returns a list with all the years with entries in
    the DB. The list is sorted in descending order.

    :param db: the bibtexparser database
    :return: list of years sorted in descending order
    """
    years = list(set([d["year"] for d in db.entries]))
    return sorted(years, reverse=True)


def get_entries(db: bibtexparser.bibdatabase.BibDatabase, year) -> list:
    """
    Given a bibtexparser DB returns the entries corresponding to a given year.

    :param db: bibtexparser DB
    :param year: year
    :return: list of entries for the given year.
    """
    return [d for d in db.entries if d["year"] == year]


def standardize_author(authorstring: str) -> str:
    """
    Normalizes the author (and editor) fields. It attempts to put the author
    name as <name> <surname>. For more than author, they are separated by
    commas. No abbreviation or manipulation of name is performed.

    :param authorstring: original author string
    :return: reformated author string
    """
    authorlist = re.split(r"\sand\s", authorstring)
    newauthorlist = []
    for a in authorlist:
        if "," in a:
            try:
                surname, name = a.split(",")
            except ValueError:
                print(f"Wrong name format:{a}")
                sys.exit(1)
            newauthorlist.append(" ".join([name.strip(), surname.strip()]))
        else:
            newauthorlist.append(a.strip())
    return ", ".join(newauthorlist)


def entry_to_html(templates: dict[str, jinja2.Template], entry: dict) -> str:
    """
    Converts a BibTeX entry to HTML using the corresponding Template.

    :param templates: dictionary of Jinja2 templates
    :param entry: the entry to convert
    :return: a string containing the HTML formatted entry
    """
    etype = entry["ENTRYTYPE"].lower()
    if etype not in templates:
        print(f"Unsupported entry type: {etype}", file=sys.stderr)
        return ""

    for k in entry:
        if k == "author" or k == "editor":
            entry[k] = standardize_author(entry[k])
    return templates[etype].render(entry)


def merge_bibfiles(bibfiles: list[str], out_filename: str) -> None:
    """
    Given a list of BibTeX files, combines them all into a single BibTeX file.

    :param bibfiles: list of BibTeX files
    :param out_filename: filename to save the merged bibtex
    """
    with open(out_filename, "wb") as fout:
        for f in bibfiles:
            with open(f, "rb") as fin:
                shutil.copyfileobj(fin, fout)


def parse_options() -> argparse.Namespace:
    """
    Parses the command line options of the script

    :return: argument or options as given by argparse.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "bibfiles", metavar="bibfile", type=str, nargs="+", help="Bibtex files"
    )
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Verbose output")
    parser.add_argument(
        "-oh",
        "--output-html",
        action="store",
        dest="output_html",
        metavar="FILENAME",
        default=FULL_HTML_FILE,
        help=f"File to save the generated HTML (default {FULL_HTML_FILE})",
    )
    parser.add_argument(
        "-ob",
        "--output-bib",
        action="store",
        dest="output_bib",
        metavar="FILENAME",
        default=FULL_BIB_FILE,
        help=f"File to save the whole bibtex file (default {FULL_BIB_FILE})",
    )
    parser.add_argument(
        "-t",
        "--template-dir",
        action="store",
        dest="template_dir",
        metavar="DIR",
        default=TEMPLATE_DIR,
        help=f"Directory containing the templates (default {TEMPLATE_DIR}",
    )
    parser.add_argument(
        "-m",
        "--template-macros",
        action='store',
        dest="macros_file",
        metavar="FILENAME",
        default=MACROS_FILE,
        help=f"Filename to be excluded from the templates directory. Usually"
        f"containing macros or helper code (default {MACROS_FILE})"
    )
    return parser.parse_args()


def main():
    args = parse_options()

    vprint = print if args.verbose else lambda *a, **k: None

    merge_bibfiles(args.bibfiles, args.output_bib)
    vprint(f"Merged {len(args.bibfiles)} bib files -> {args.output_bib}")

    templates = load_templates(args.template_dir, args.macros_file)
    vprint(f"Loaded templates for: {templates.keys()}")

    db = read_bibtex_file(args.output_bib)
    vprint(f"Loaded {len(db.entries)} entries from {args.output_bib}")
    years = get_all_years(db)
    out = ""
    vprint(f"Found entries for {len(years)} years")
    for y in years:
        out += f'<h3 class="bibheader">{y}</h3>\n<ul class="biblist">\n'
        entries = get_entries(db, y)
        for e in entries:
            out += '<li class="bibitem">'
            out += entry_to_html(templates, e)
            out += "</li>\n"
        out += "</ul>\n"
        vprint(f" -> {y} done")
    with open(args.output_html, "w") as f:
        f.write(out)
        vprint(f"Output html saved -> {args.output_html}")


if __name__ == "__main__":
    main()
